## kotlink

A API Server writtern by kotlin.

## feature

- Spot CRUDS
  - 行ったことがあるお店の情報を記録できるようにします。

## schema

- スキーマ定義
  - spot
    - id(auto increment)  : integer
    - spot_name : text
    - spot_time : text
      - morning/lunch/dinnerなど
    - traffic_method : text
    - address : text
      - 住所
    - tels : text (Delimiter: .)
    - urls : text (Delimiter: .)
    - category : text
      - 肉/寿司などのカテゴリ
    - tag : text
      - バエるなどのタグ
    - status : text
      - 未訪問/訪問済みなど
    - details : text
      - フリーワードテキスト
  - spot_records
    - id : integer primary key
    - spot_id : integer(spot.id)
    - date : text
      - 日付
    - members : text
      - 人数
    - note : text
      - ノート

## frontend

- [Vue.js](https://jp.vuejs.org/index.html) 

## misc

- use [springboot](https://spring.io/projects/spring-boot)
- use [Exposed](https://github.com/JetBrains/Exposed) 
  - [ここ](https://github.com/JetBrains/Exposed/blob/master/src/test/kotlin/demo/SamplesDao.kt)を参考に

