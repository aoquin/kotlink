package net.aoquin.kotlink.bean.spot

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object SpotRecords : IntIdTable( name = "spot_records" ) {
    val spotId = integer( name = "spot_id" )
    val date = varchar( name = "date", length = 128)
    val members = varchar( name = "members", length = 128)
    val note = varchar( name = "note", length = 128)
}

class SpotRecord(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<SpotRecord>(SpotRecords)

    var spotId by SpotRecords.spotId
    var date by SpotRecords.date
    var members by SpotRecords.members
    var note by SpotRecords.note
}

