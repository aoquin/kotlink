package net.aoquin.kotlink.bean.spot

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Spots : IntIdTable( name = "spot" ) {
    val spotName = varchar( name = "spot_name", length = 128 )
    val spotTime = varchar( name = "spot_time", length = 128)
    val trafficMethod = varchar( name = "traffic_method", length = 128)
    val address = varchar( name = "address", length = 128)
    val tels = varchar( name = "tels", length = 128)
    val urls = varchar( name = "urls", length = 128)
    val category = varchar( name = "category", length = 128)
    val tag = varchar( name = "tag", length = 128)
    val status = varchar( name = "status", length = 128)
    val details = varchar( name = "details", length = 128)
}

class Spot(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Spot>(Spots)

    var spotName by Spots.spotName
    var spotTime by Spots.spotTime
    var trafficMethod by Spots.trafficMethod
    var address by Spots.address
    var tels by Spots.tels
    var urls by Spots.urls
    var category by Spots.category
    var tag by Spots.tag
    var status by Spots.status
    var details by Spots.details
}

