package net.aoquin.kotlink.response

data class GenericResponse(val message: String, val status: Int, val item: Any?)