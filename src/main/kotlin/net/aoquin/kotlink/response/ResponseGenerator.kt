package net.aoquin.kotlink.response

import com.fasterxml.jackson.databind.ObjectMapper

class ResponseGenerator {

    var mapper = ObjectMapper()

    fun write(message: String, status: Int, item: Any?): String {

        return mapper.writeValueAsString(GenericResponse(
                message = message,
                status = status,
                item = item))

    }

}