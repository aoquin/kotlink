package net.aoquin.kotlink.response

data class SpotRecordResponse(val spotId: Int, val date: String, val members: String, val note: String)

