package net.aoquin.kotlink.response

data class SpotResponse(val id: Int, val spotName: String, val spotTime: String, val trafficMethod: String,
                        val address: String, val tels: String, val urls: String, val category:String,
                        val tag: String, val status: String, val details: String)

