package net.aoquin.kotlink.dao

import net.aoquin.kotlink.bean.spot.SpotRecord
import net.aoquin.kotlink.bean.spot.SpotRecords
import net.aoquin.kotlink.response.SpotRecordResponse
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SpotRecordDao {

    fun findAll() : List<SpotRecordResponse> {

        var responses = mutableListOf<SpotRecordResponse>()

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_READ_UNCOMMITTED

        transaction {

            SchemaUtils.create (SpotRecords)
            addLogger(StdOutSqlLogger)

            // select * from spot
            val records = SpotRecord.all()

            for(record in records) {

                responses.add(SpotRecordResponse(
                        spotId = record.spotId,
                        date = record.date,
                        members = record.members,
                        note = record.note
                ))

            }

        }

        return responses
    }

    fun find(id: String): SpotRecordResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_READ_UNCOMMITTED

        var response: SpotRecordResponse? = null
        transaction {

            addLogger(StdOutSqlLogger)
            val record = SpotRecord.findById(id.toInt())

            if (record != null) {

                response = SpotRecordResponse(
                    spotId = record.spotId,
                    date = record.date,
                    members = record.members,
                    note = record.note
                )

            }

        }

        return response

    }

    fun insert(spotId: String, members: String, note: String): SpotRecordResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: SpotRecord? = null
        transaction {

            record = SpotRecord.new {
                this.spotId = spotId.toInt()
                this.date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"))
                this.members = if (members.isEmpty()) "" else members
                this.note = if (note.isEmpty()) "" else note
            }

        }

        return SpotRecordResponse(
                spotId = record!!.spotId,
                date = record!!.date,
                members = record!!.members,
                note = record!!.note
        )

    }

    fun update(id: String, spotId: String, members: String, note: String): SpotRecordResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: SpotRecord? = null
        transaction {

            // select * from spot where id = ?
            record = SpotRecord.findById(id.toInt())
            if (!spotId.isEmpty()) record?.spotId = spotId.toInt()
            if (!members.isEmpty()) record?.members = members
            if (!note.isEmpty()) record?.note = note

        }

        return SpotRecordResponse(
                spotId = record!!.spotId,
                date = record!!.date,
                members = record!!.members,
                note = record!!.note
        )

    }

    fun delete(id: String): SpotRecordResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: SpotRecord? = null
        transaction {
            record = SpotRecord.findById(id.toInt())
            record?.delete()
        }

        return SpotRecordResponse(
                spotId = record!!.spotId,
                date = record!!.date,
                members = record!!.members,
                note = record!!.note
        )

    }

}