package net.aoquin.kotlink.dao

import net.aoquin.kotlink.bean.spot.Spot
import net.aoquin.kotlink.bean.spot.SpotRecord
import net.aoquin.kotlink.bean.spot.Spots
import net.aoquin.kotlink.response.SpotRecordResponse
import net.aoquin.kotlink.response.SpotResponse
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SpotDao {

    fun findAll(): List<SpotResponse> {

        var responses = mutableListOf<SpotResponse>()

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_READ_UNCOMMITTED

        transaction {

            SchemaUtils.create (Spots)
            addLogger(StdOutSqlLogger)

            // select * from spot
            val spots = Spot.all()

            for(spot in spots) {

                responses.add(SpotResponse(
                        id = spot.id.value,
                        spotName = spot.spotName,
                        spotTime = spot.spotTime,
                        trafficMethod = spot.trafficMethod,
                        address = spot.address,
                        tels = spot.tels,
                        urls = spot.urls,
                        category = spot.category,
                        tag = spot.tag,
                        status = spot.status,
                        details = spot.details
                ))

            }

        }

        return responses
    }

    fun find(id: String): SpotResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_READ_UNCOMMITTED

        var response: SpotResponse? = null
        transaction {

            addLogger(StdOutSqlLogger)
            val spot = Spot.findById(id.toInt())

            if (spot != null) {

                response = SpotResponse(
                        id = spot.id.value,
                        spotName = spot.spotName,
                        spotTime = spot.spotTime,
                        trafficMethod = spot.trafficMethod,
                        address = spot.address,
                        tels = spot.tels,
                        urls = spot.urls,
                        category = spot.category,
                        tag = spot.tag,
                        status = spot.status,
                        details = spot.details
                )

            }

        }

        return response
    }

    fun insert(spotName: String, spotTime: String, trafficMethod: String
               , address: String, tels: String, urls: String, category: String
               , tag: String, status: String, details: String): SpotResponse? {
        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: Spot? = null
        transaction {

            record = Spot.new {
                this.spotName = spotName
                this.spotTime = spotTime
                this.trafficMethod = trafficMethod
                this.address = address
                this.tels = tels
                this.urls = urls
                this.category = category
                this.tag = tag
                this.status = status
                this.details = details
            }

        }

        return SpotResponse(
                id = record!!.id.value,
                spotName = record!!.spotName,
                spotTime = record!!.spotTime,
                trafficMethod = record!!.trafficMethod,
                address = record!!.address,
                tels = record!!.tels,
                urls = record!!.urls,
                category = record!!.category,
                tag = record!!.tag,
                status = record!!.status,
                details = record!!.details
        )
    }

    fun update(id: String, spotName: String, spotTime: String, trafficMethod: String
               , address: String, tels: String, urls: String, category: String
               , tag: String, status: String, details: String): SpotResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: Spot? = null
        transaction {

            // select * from spot where id = ?
            record = Spot.findById(id.toInt())
            if (!spotName.isEmpty()) record?.spotName = spotName
            if (!spotTime.isEmpty()) record?.spotTime = spotTime
            if (!trafficMethod.isEmpty()) record?.trafficMethod = trafficMethod
            if (!address.isEmpty()) record?.address = address
            if (!tels.isEmpty()) record?.tels = tels
            if (!urls.isEmpty()) record?.urls = urls
            if (!category.isEmpty()) record?.category = category
            if (!tag.isEmpty()) record?.tag = tag
            if (!status.isEmpty()) record?.status = status
            if (!details.isEmpty()) record?.details = details

        }

        return SpotResponse(
                id = record!!.id.value,
                spotName = record!!.spotName,
                spotTime = record!!.spotTime,
                trafficMethod = record!!.trafficMethod,
                address = record!!.address,
                tels = record!!.tels,
                urls = record!!.urls,
                category = record!!.category,
                tag = record!!.tag,
                status = record!!.status,
                details = record!!.details
        )
    }

    fun delete(id: String): SpotResponse? {

        // open spot_db
        Database.connect("jdbc:sqlite:spot_db", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

        var record: Spot? = null
        transaction {
            record = Spot.findById(id.toInt())
            record?.delete()
        }

        return SpotResponse(
                id = record!!.id.value,
                spotName = record!!.spotName,
                spotTime = record!!.spotTime,
                trafficMethod = record!!.trafficMethod,
                address = record!!.address,
                tels = record!!.tels,
                urls = record!!.urls,
                category = record!!.category,
                tag = record!!.tag,
                status = record!!.status,
                details = record!!.details
        )

    }
}