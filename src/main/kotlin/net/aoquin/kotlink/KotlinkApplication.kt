package net.aoquin.kotlink

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KotlinkApplication

fun main(args: Array<String>) {
	runApplication<KotlinkApplication>(*args)
}
