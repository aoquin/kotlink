package net.aoquin.kotlink

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.transactions.TransactionManager
import java.sql.Connection

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping

@RestController
@RequestMapping("hello")
class HelloController {
    @GetMapping("")
    fun index(): String {
        return "Hello World!!"
    }

    @GetMapping("database")
    fun database(): String {
        Database.connect("jdbc:sqlite:/Users/aoquin/src/kotlin/kotlink/ex1", "org.sqlite.JDBC")
        TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
        transaction {
            addLogger(StdOutSqlLogger)
            SchemaUtils.create (tbl1)
  
            // 'select *' SQL: SELECT tbl1.one, tbl1.two FROM tbl1
            println("tbl1: ${tbl1.selectAll()}")
        }
        return "Database accessed!"
    }
}

object tbl1: Table() {
    val one = varchar("one", 10)
    val two = integer("two")
}

