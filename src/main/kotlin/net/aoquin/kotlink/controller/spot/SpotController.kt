package net.aoquin.kotlink.controller.spot

import com.fasterxml.jackson.databind.ObjectMapper
import net.aoquin.kotlink.dao.SpotDao
import net.aoquin.kotlink.dao.SpotRecordDao
import net.aoquin.kotlink.response.ResponseGenerator
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("spotRecords")
class SpotController {

    var spotDao = SpotDao()

    var generator = ResponseGenerator()

    var mapper = ObjectMapper()

    @GetMapping("")
    fun index(): String {

        return generator.write(
                message = spotDao.findAll().joinToString { "id: " + it.id + ", name: " + it.spotName },
                status = 200,
                item = spotDao.findAll())

    }

    @GetMapping("find")
    fun index(@RequestParam id: String): String {

        return generator.write(
                message = mapper.writeValueAsString(spotDao.find(id) ?: ""),
                status = 200,
                item = spotDao.find(id))

    }

    @PostMapping("")
    fun post(@RequestParam spotId: String,
             @RequestParam(defaultValue = "") members: String,
             @RequestParam(defaultValue = "") note: String): String {

        return generator.write(
                message = "posted!",
                status = 200,
                item = spotDao.insert(spotId, members, note))

    }

    @PutMapping("")
    fun update( @RequestParam id: String,
             @RequestParam(defaultValue = "") spotId: String,
             @RequestParam(defaultValue = "") members: String,
             @RequestParam(defaultValue = "") note: String): String {

        return generator.write(
                message = "updated!",
                status = 200,
                item = spotDao.update(id, spotId, members, note))

    }

    @DeleteMapping("")
    fun delete( @RequestParam id: String): String {

        return generator.write(
                message = "deleted!",
                status = 200,
                item = spotDao.delete(id))

    }

}