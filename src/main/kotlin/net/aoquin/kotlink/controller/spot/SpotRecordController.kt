package net.aoquin.kotlink.controller.spot

import com.fasterxml.jackson.databind.ObjectMapper
import net.aoquin.kotlink.dao.SpotRecordDao
import net.aoquin.kotlink.response.ResponseGenerator
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("spotRecords")
class SpotRecordController {

    var spotRecordDao = SpotRecordDao()

    var generator = ResponseGenerator()

    var mapper = ObjectMapper()

    @GetMapping("")
    fun index(): String {

        return generator.write(
                message = spotRecordDao.findAll().joinToString { "name: " + it.spotId + ", members: " + it.members },
                status = 200,
                item = spotRecordDao.findAll())

    }

    @GetMapping("find")
    fun index(@RequestParam id: String): String {

        return generator.write(
                message = mapper.writeValueAsString(spotRecordDao.find(id) ?: ""),
                status = 200,
                item = spotRecordDao.find(id))

    }

    @PostMapping("")
    fun post(@RequestParam spotId: String,
             @RequestParam(defaultValue = "") members: String,
             @RequestParam(defaultValue = "") note: String): String {

        return generator.write(
                message = "posted!",
                status = 200,
                item = spotRecordDao.insert(spotId, members, note))

    }

    @PutMapping("")
    fun update( @RequestParam id: String,
             @RequestParam(defaultValue = "") spotId: String,
             @RequestParam(defaultValue = "") members: String,
             @RequestParam(defaultValue = "") note: String): String {

        return generator.write(
                message = "updated!",
                status = 200,
                item = spotRecordDao.update(id, spotId, members, note))

    }

    @DeleteMapping("")
    fun delete( @RequestParam id: String): String {

        return generator.write(
                message = "deleted!",
                status = 200,
                item = spotRecordDao.delete(id))

    }

}